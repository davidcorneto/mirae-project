import 'package:project_mirae/src/models/user_response_model.dart';
import 'package:project_mirae/src/repository/api_helper.dart';

class Repository{

  ApiHelper apiHelper = ApiHelper();

  static const String LISTUSER = 'search/users?q=ade&per_page=10/';

  Future<UserDataResponseModel> userList({String keyword = 'Ade'}) async {
    final response = await apiHelper.get('search/users?q=${keyword ?? 'Ade'}&per_page=10/');
    final result = UserDataResponseModel.fromJson(response);
    return result;
  }
}