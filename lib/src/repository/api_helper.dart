import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as client;
import 'package:project_mirae/src/repository/config.dart';
import 'package:project_mirae/src/utilities/app_exceptions.dart';

class ApiHelper {
  ApiHelper._instantiate();

  ApiHelper();

  static final ApiHelper instance = ApiHelper._instantiate();

  List<String> baseUrl = getBaseUrl();

  var header = new Map<String, String>();

  static List<String> getBaseUrl() {
    return Configs.baseUrl;
  }

  static Duration _getTimeOutDuration() {
    return Configs.timeOutDuration;
  }

  Future<Map<String, String>> getHeader() async {
    var result = Map<String, String>();
    result["Content-Type"] = "application/json";
    print('header : $result');
    return result;
  }

  Future<dynamic> get(String url,
      {dynamic params, int baseUrlIndex = 0}) async {
    print('Api Get, url ${baseUrl[baseUrlIndex] + url}');
    print(url);
    String _url = baseUrl[baseUrlIndex] + url;

    var responseJson;
    try {
      final response = await client
          .get(Uri.parse(_url), headers: await getHeader())
          .timeout(_getTimeOutDuration());

      print('RESPONSE ${response.body}');
      responseJson = _returnResponse(response);
    } on SocketException {
      print('No net');
      throw FetchDataException('No Internet connection');
    } on TimeoutException {
      print(
          'Koneksi Anda lemah, Pastikan internet Anda lancar');
      throw FetchDataException(
          'Koneksi Anda lemah, Pastikan internet Anda lancar');
    }

    print('api get recieved!');
    return responseJson;
  }

  dynamic _returnResponse(client.Response response) {
    //TODO: Bikin force logout dari utilities force logout
    switch (response.statusCode) {
      case 200:
        var responseJson = json.decode(response.body.toString());
        // if (responseJson["status"].toString() == 'error') {
        //   if (responseJson["data"] != null) {
        //     if ((responseJson["message"] ?? '')
        //             .toString()
        //             .toLowerCase()
        //             .contains('invalid username or password')) {
        //       throw Exception(responseJson["message"] ?? '');
        //     }
        //   } else {
        //     throw Exception('Oops! Something Went Wrong...');
        //   }
        // }
        return responseJson;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:
      case 403:
        throw UnauthorisedException(response.body.toString());
      case 413:
        throw UnauthorisedException('Ukuran file terlalu besar');
      case 500:
      case 502:
      default:
        throw FetchDataException(
            'Error occured while Communication with Server.\nStatusCode :${response.statusCode}');
    }
  }

}