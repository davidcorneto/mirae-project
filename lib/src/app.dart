import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:project_mirae/src/blocs/bloc_provider.dart';
import 'package:project_mirae/src/blocs/home_bloc.dart';
import 'package:project_mirae/src/screen/home_screen.dart';
import 'package:project_mirae/src/utilities/utilities.dart';

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> with WidgetsBindingObserver{
  @override
  initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }


  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Mirae Apps',
      theme: ThemeData(fontFamily: 'Nunito', brightness: Brightness.light),
      onGenerateRoute: routes,
      navigatorKey: Utilities.navigatorKey,
    );
  }

  static Route<dynamic> routes(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        final bloc = HomeBloc();
        return MaterialPageRoute(
          builder: (context) {
            return BlocProvider<HomeBloc>(
              bloc: bloc,
              child: HomeScreen(),
            );
          },
        );
      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            body: Center(
              child: Text('No route defined for ${settings.name}'),
            ),
          ),
        );
    }
  }
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.inactive:
        debugPrint("didChangeAppLifecycleState: App inactive");
        // TODO: Handle this case.
        break;
      case AppLifecycleState.resumed:
        debugPrint("didChangeAppLifecycleState: App resumed");
        break;
      case AppLifecycleState.paused:
        debugPrint("didChangeAppLifecycleState: App paused");
        break;
      case AppLifecycleState.detached:
        debugPrint("didChangeAppLifecycleState: App detached");
        // TODO: Handle this case.
        break;
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }
}