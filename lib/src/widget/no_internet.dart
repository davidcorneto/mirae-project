import 'package:flutter/material.dart';
import 'package:project_mirae/src/constant/custom_colors.dart';

class NoInternet extends StatefulWidget {
  final Function onClicked;
  NoInternet({this.onClicked});
  @override
  _NoInternetState createState() => _NoInternetState();
}

class _NoInternetState extends State<NoInternet> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'Error',
          ),
          FlatButton(
            onPressed: () {
              if (widget.onClicked != null) widget.onClicked();
            },
            color: CustomColors.text400,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0),
            ),
            child: Text(
              'Retry',
              style: TextStyle(
                fontWeight: FontWeight.w800,
                fontSize: 12,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}