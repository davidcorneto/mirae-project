
import 'package:flutter/material.dart';
import 'package:project_mirae/src/constant/custom_colors.dart';

class LoadingWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: CircularProgressIndicator(
        backgroundColor: Colors.transparent,
        valueColor: AlwaysStoppedAnimation(CustomColors.grey200),
        strokeWidth: 3,
      ),
    );
  }
}