import 'package:flutter/material.dart';
import 'package:project_mirae/src/blocs/bloc_provider.dart';
import 'package:project_mirae/src/blocs/home_bloc.dart';
import 'package:project_mirae/src/constant/custom_colors.dart';
import 'package:project_mirae/src/models/user_response_model.dart';
import 'package:project_mirae/src/repository/api_response.dart';
import 'package:project_mirae/src/widget/loading_widget.dart';
import 'package:project_mirae/src/widget/no_data.dart';
import 'package:project_mirae/src/widget/no_internet.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<HomeBloc>(context);
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('USER DATA'),
        centerTitle: true,
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: [
              TextField(
                controller: bloc.txtSearch,
                onChanged: (val){
                  bloc.getData(val);
                },
              ),
              commonBody(
                size: size,
                bloc: bloc,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget commonBody({HomeBloc bloc, Size size}) {
    return StreamBuilder<ApiResponse<UserDataResponseModel>>(
      stream: bloc.listUser,
      builder: (context, snapshot) {
        final data = snapshot?.data;
        print('DATA ${data?.data?.items.toString()}');
        print('STATUS ${data?.status.toString()}');
        switch (data?.status) {
          case Status.LOADING:
            return LoadingWidget();
            break;
          case Status.COMPLETED:
            final user = data?.data ?? [];
            if (data?.data?.items?.length == 0) return noData(size: size);
            return ListView.separated(
              padding:
                  EdgeInsets.only(bottom: 25, left: 16, right: 16, top: 16),
              separatorBuilder: (context, index) {
                return SizedBox(
                  height: 8,
                );
              },
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: data?.data?.items?.length,
              itemBuilder: (context, index) {
                return Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        data?.data?.items[index]?.login ?? '',
                      ),
                    ],
                  ),
                );
              },
            );
            break;
          case Status.ERROR:
            print('Error');
            return Text(data.message);
          break;
          default:
            print('Default Error');
            return Container();
            break;
        }
      },
    );
  }

  Widget itemWidget({
    Size size,
    String name,
    String image,
    String role,
    String primaryAttr,
    Function onClicked,
  }) {
    name = name ?? '';
    image = image ?? '';
    role = role ?? '';
    primaryAttr = primaryAttr ?? '';

    return Column(
      children: [
        GestureDetector(
          onTap: () {
            if (onClicked != null) onClicked();
          },
          child: Hero(
            tag: name,
            child: Container(
              width: size.width * 0.4,
              height: size.width * 0.4,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: image.isNotEmpty
                    ? CustomColors.text200
                    : CustomColors.AgrikuColor1,
              ),
              child: image.isNotEmpty
                  ? ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: Image.network(
                        image,
                        fit: BoxFit.cover,
                      ),
                    )
                  : Center(
                      child: Container(
                        height: 50,
                        width: 50,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage('assets/images/ic_image.png'),
                            fit: BoxFit.contain,
                          ),
                        ),
                      ),
                    ),
            ),
          ),
        ),
        SizedBox(height: 16),
        Flexible(
          child: Container(
            width: size.width * 0.4,
            // height: size.width * 0.15,
            // color: Colors.red,
            child: Text(
              '$name',
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w400,
                color: CustomColors.text500,
              ),
            ),
          ),
        ),
        Container(
          width: size.width * 0.4,
          child: Text(
            '$role',
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w800,
              color: CustomColors.text200,
            ),
          ),
        ),
      ],
    );
  }
}
