import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:project_mirae/src/blocs/bloc.dart';
import 'package:project_mirae/src/models/user_response_model.dart';
import 'package:project_mirae/src/repository/api_response.dart';
import 'package:project_mirae/src/repository/repository.dart';
import 'package:rxdart/rxdart.dart';

class HomeBloc implements Bloc{
  @override
  BuildContext context;

  Repository _repository = Repository();

  final txtSearch = TextEditingController();

  final _listUser = BehaviorSubject<ApiResponse<UserDataResponseModel>>();

  Stream<ApiResponse<UserDataResponseModel>> get listUser =>
      _listUser.stream;

  Function(ApiResponse<UserDataResponseModel>) get addListUser =>
      _listUser.sink.add;

  ApiResponse<UserDataResponseModel> get listUserValue =>
      _listUser.value;

  HomeBloc(){
    getData('A');
  }

  getData(String val) async{
    addListUser(ApiResponse.loading());
    try {
      // var keyword = txtSearch.value.toString();
      var keyword = val ?? 'A';
      final response = await _repository.userList(keyword: keyword);
      addListUser(ApiResponse.completed(response));
    } catch (e) {
      addListUser(ApiResponse.error(e.toString()));
    }
  }

  itemClicked(String userId) {

  }

  @override
  void dispose() {
    // TODO: implement dispose
    _listUser.close();
  }
}